# A3C Starter Codes
from OpenAI universe-starter-agent
(https://github.com/openai/universe-starter-agent)

This folder is the state-of-the-art network.

### Experimental 
This repo is used for experimental task.


### Usage
Launch 4 workers and 1 policy monitor.
```
python distribute_tasks.py -w 4
```